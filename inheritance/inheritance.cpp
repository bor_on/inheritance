#include <iostream>

class Animal
{
public:
    void virtual Voice()
    {
        std::cout << "Voice\n";
    }
};

class Dog : public Animal
{
 public:
    void Voice()
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void Voice()
    {
        std::cout << "Mrrr\n";
    }
};

class Bird : public Animal
{
public:
    void Voice()
    {
        std::cout << "chirik\n";
    }
};

int main()
{
    Animal* mass[4] = { new Animal, new Dog, new Cat, new Bird };
    for (int i = 0; i < 4; i++)
    {
        mass[i]->Voice();
    }
}